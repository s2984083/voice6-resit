PGDMP  8    #                |           voice6Development    16.3    16.3 m    j           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            k           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            l           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            m           1262    16861    voice6Development    DATABASE     �   CREATE DATABASE "voice6Development" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';
 #   DROP DATABASE "voice6Development";
                postgres    false            �            1259    16862    map    TABLE     �   CREATE TABLE public.map (
    project_id integer NOT NULL,
    longitude double precision NOT NULL,
    latitude double precision NOT NULL,
    radius integer
);
    DROP TABLE public.map;
       public         heap    postgres    false            �            1259    16865    news    TABLE     �   CREATE TABLE public.news (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    description text NOT NULL,
    category character varying NOT NULL,
    created_at date DEFAULT now() NOT NULL
);
    DROP TABLE public.news;
       public         heap    postgres    false            �            1259    16871    news_id_seq    SEQUENCE     �   ALTER TABLE public.news ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    216            �            1259    16872 
   news_image    TABLE     �   CREATE TABLE public.news_image (
    id integer NOT NULL,
    news_id integer NOT NULL,
    path character varying(500) NOT NULL
);
    DROP TABLE public.news_image;
       public         heap    postgres    false            �            1259    16877    news_image_id_seq    SEQUENCE     �   ALTER TABLE public.news_image ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.news_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    218            �            1259    16878 	   news_vote    TABLE     �   CREATE TABLE public.news_vote (
    id integer NOT NULL,
    choice boolean NOT NULL,
    news_id integer NOT NULL,
    user_id integer NOT NULL,
    created_at date DEFAULT now() NOT NULL
);
    DROP TABLE public.news_vote;
       public         heap    postgres    false            �            1259    16882    news_vote_id_seq    SEQUENCE     �   ALTER TABLE public.news_vote ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.news_vote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    220            �            1259    16883    product    TABLE     u  CREATE TABLE public.product (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    description character varying(500) NOT NULL,
    category character varying NOT NULL,
    price numeric(6,2) NOT NULL,
    created_at date DEFAULT now() NOT NULL,
    available_pickup boolean DEFAULT true NOT NULL,
    available_delivery boolean DEFAULT false NOT NULL
);
    DROP TABLE public.product;
       public         heap    postgres    false            �            1259    16891    product_clothing    TABLE     �   CREATE TABLE public.product_clothing (
    id integer NOT NULL,
    product_id integer NOT NULL,
    sizes jsonb NOT NULL,
    colors jsonb NOT NULL
);
 $   DROP TABLE public.product_clothing;
       public         heap    postgres    false            �            1259    16896    product_clothing_id_seq    SEQUENCE     �   ALTER TABLE public.product_clothing ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.product_clothing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    223            �            1259    16897    product_id_seq    SEQUENCE     �   ALTER TABLE public.product ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    222            �            1259    16898    product_image    TABLE     �   CREATE TABLE public.product_image (
    id integer NOT NULL,
    product_id integer NOT NULL,
    path character varying(500) NOT NULL
);
 !   DROP TABLE public.product_image;
       public         heap    postgres    false            �            1259    16903    product_image_id_seq    SEQUENCE     �   ALTER TABLE public.product_image ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.product_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    226            �            1259    16904    product_order    TABLE     �   CREATE TABLE public.product_order (
    id integer NOT NULL,
    product_id integer,
    quantity integer NOT NULL,
    properties jsonb,
    group_id integer NOT NULL
);
 !   DROP TABLE public.product_order;
       public         heap    postgres    false            �            1259    16909    product_order_group    TABLE       CREATE TABLE public.product_order_group (
    id integer NOT NULL,
    user_id integer,
    created_at date DEFAULT now() NOT NULL,
    delivery_address character varying(500),
    tracking_number character varying(500),
    fulfilled boolean DEFAULT false NOT NULL
);
 '   DROP TABLE public.product_order_group;
       public         heap    postgres    false            �            1259    16916    product_order_group_id_seq    SEQUENCE     �   ALTER TABLE public.product_order_group ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.product_order_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    229            �            1259    16917    product_order_id_seq    SEQUENCE     �   ALTER TABLE public.product_order ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.product_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    228            �            1259    16918    profile    TABLE       CREATE TABLE public.profile (
    user_id integer NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    birth_date date NOT NULL,
    gender character varying(50) NOT NULL,
    hide_on_leaderboard boolean DEFAULT false NOT NULL
);
    DROP TABLE public.profile;
       public         heap    postgres    false            �            1259    16922    project    TABLE     3  CREATE TABLE public.project (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    description character varying(500) NOT NULL,
    status boolean DEFAULT false,
    user_id integer NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    votes_required integer DEFAULT 1000,
    reward integer DEFAULT 10,
    created_at date DEFAULT now() NOT NULL,
    category character varying DEFAULT 'Other'::character varying NOT NULL,
    municipality_project boolean DEFAULT false NOT NULL,
    public boolean DEFAULT false NOT NULL
);
    DROP TABLE public.project;
       public         heap    postgres    false            �            1259    16934    project_image    TABLE     �   CREATE TABLE public.project_image (
    id integer NOT NULL,
    project_id integer NOT NULL,
    path character varying(500) NOT NULL
);
 !   DROP TABLE public.project_image;
       public         heap    postgres    false            �            1259    16939    project_image_id_seq    SEQUENCE     �   ALTER TABLE public.project_image ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    234            �            1259    16940    project_question    TABLE     �   CREATE TABLE public.project_question (
    id integer NOT NULL,
    project_id integer NOT NULL,
    content character varying NOT NULL
);
 $   DROP TABLE public.project_question;
       public         heap    postgres    false            �            1259    16945    project_question_answer    TABLE     �   CREATE TABLE public.project_question_answer (
    id integer NOT NULL,
    question_id integer NOT NULL,
    answer_id integer NOT NULL,
    user_id integer NOT NULL
);
 +   DROP TABLE public.project_question_answer;
       public         heap    postgres    false            �            1259    16948    project_question_answer_id_seq    SEQUENCE     �   ALTER TABLE public.project_question_answer ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_question_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    237            �            1259    16949    project_question_id_seq    SEQUENCE     �   ALTER TABLE public.project_question ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    236            �            1259    16950    project_question_option    TABLE     �   CREATE TABLE public.project_question_option (
    id integer NOT NULL,
    question_id integer NOT NULL,
    content character varying NOT NULL
);
 +   DROP TABLE public.project_question_option;
       public         heap    postgres    false            �            1259    16955    project_question_option_id_seq    SEQUENCE     �   ALTER TABLE public.project_question_option ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.project_question_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    240            �            1259    16956    projects_id_seq    SEQUENCE     �   ALTER TABLE public.project ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    233            �            1259    16957    user    TABLE     �   CREATE TABLE public."user" (
    id integer NOT NULL,
    phone character varying(50) NOT NULL,
    password character varying(60) NOT NULL,
    admin boolean DEFAULT false NOT NULL
);
    DROP TABLE public."user";
       public         heap    postgres    false            �            1259    16961    user_id_seq    SEQUENCE     �   ALTER TABLE public."user" ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    243            �            1259    16962    user_points    TABLE     f   CREATE TABLE public.user_points (
    user_id integer NOT NULL,
    total_points integer DEFAULT 0
);
    DROP TABLE public.user_points;
       public         heap    postgres    false            �            1259    16966    vote    TABLE     �   CREATE TABLE public.vote (
    user_id integer NOT NULL,
    project_id integer NOT NULL,
    id integer NOT NULL,
    created_at date DEFAULT now() NOT NULL,
    choice boolean NOT NULL
);
    DROP TABLE public.vote;
       public         heap    postgres    false            �            1259    16970    vote_id_seq    SEQUENCE     �   ALTER TABLE public.vote ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.vote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    246            G          0    16862    map 
   TABLE DATA           F   COPY public.map (project_id, longitude, latitude, radius) FROM stdin;
    public          postgres    false    215   ��       H          0    16865    news 
   TABLE DATA           L   COPY public.news (id, title, description, category, created_at) FROM stdin;
    public          postgres    false    216   ��       J          0    16872 
   news_image 
   TABLE DATA           7   COPY public.news_image (id, news_id, path) FROM stdin;
    public          postgres    false    218   Ȍ       L          0    16878 	   news_vote 
   TABLE DATA           M   COPY public.news_vote (id, choice, news_id, user_id, created_at) FROM stdin;
    public          postgres    false    220   �       N          0    16883    product 
   TABLE DATA           {   COPY public.product (id, name, description, category, price, created_at, available_pickup, available_delivery) FROM stdin;
    public          postgres    false    222   J�       O          0    16891    product_clothing 
   TABLE DATA           I   COPY public.product_clothing (id, product_id, sizes, colors) FROM stdin;
    public          postgres    false    223   ��       R          0    16898    product_image 
   TABLE DATA           =   COPY public.product_image (id, product_id, path) FROM stdin;
    public          postgres    false    226   �       T          0    16904    product_order 
   TABLE DATA           W   COPY public.product_order (id, product_id, quantity, properties, group_id) FROM stdin;
    public          postgres    false    228   ��       U          0    16909    product_order_group 
   TABLE DATA           t   COPY public.product_order_group (id, user_id, created_at, delivery_address, tracking_number, fulfilled) FROM stdin;
    public          postgres    false    229   k�       X          0    16918    profile 
   TABLE DATA           j   COPY public.profile (user_id, first_name, last_name, birth_date, gender, hide_on_leaderboard) FROM stdin;
    public          postgres    false    232   �       Y          0    16922    project 
   TABLE DATA           �   COPY public.project (id, title, description, status, user_id, start_date, end_date, votes_required, reward, created_at, category, municipality_project, public) FROM stdin;
    public          postgres    false    233   ��       Z          0    16934    project_image 
   TABLE DATA           =   COPY public.project_image (id, project_id, path) FROM stdin;
    public          postgres    false    234   ��       \          0    16940    project_question 
   TABLE DATA           C   COPY public.project_question (id, project_id, content) FROM stdin;
    public          postgres    false    236   ��       ]          0    16945    project_question_answer 
   TABLE DATA           V   COPY public.project_question_answer (id, question_id, answer_id, user_id) FROM stdin;
    public          postgres    false    237   @�       `          0    16950    project_question_option 
   TABLE DATA           K   COPY public.project_question_option (id, question_id, content) FROM stdin;
    public          postgres    false    240   |�       c          0    16957    user 
   TABLE DATA           <   COPY public."user" (id, phone, password, admin) FROM stdin;
    public          postgres    false    243   ��       e          0    16962    user_points 
   TABLE DATA           <   COPY public.user_points (user_id, total_points) FROM stdin;
    public          postgres    false    245   ��       f          0    16966    vote 
   TABLE DATA           K   COPY public.vote (user_id, project_id, id, created_at, choice) FROM stdin;
    public          postgres    false    246   ך       n           0    0    news_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.news_id_seq', 9, true);
          public          postgres    false    217            o           0    0    news_image_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.news_image_id_seq', 1, true);
          public          postgres    false    219            p           0    0    news_vote_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.news_vote_id_seq', 2, true);
          public          postgres    false    221            q           0    0    product_clothing_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.product_clothing_id_seq', 5, true);
          public          postgres    false    224            r           0    0    product_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.product_id_seq', 21, true);
          public          postgres    false    225            s           0    0    product_image_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.product_image_id_seq', 9, true);
          public          postgres    false    227            t           0    0    product_order_group_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.product_order_group_id_seq', 20, true);
          public          postgres    false    230            u           0    0    product_order_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.product_order_id_seq', 30, true);
          public          postgres    false    231            v           0    0    project_image_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.project_image_id_seq', 16, true);
          public          postgres    false    235            w           0    0    project_question_answer_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.project_question_answer_id_seq', 21, true);
          public          postgres    false    238            x           0    0    project_question_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.project_question_id_seq', 23, true);
          public          postgres    false    239            y           0    0    project_question_option_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.project_question_option_id_seq', 35, true);
          public          postgres    false    241            z           0    0    projects_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.projects_id_seq', 38, true);
          public          postgres    false    242            {           0    0    user_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.user_id_seq', 33, true);
          public          postgres    false    244            |           0    0    vote_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.vote_id_seq', 24, true);
          public          postgres    false    247                       2606    16972    map Map_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.map
    ADD CONSTRAINT "Map_pkey" PRIMARY KEY (project_id);
 8   ALTER TABLE ONLY public.map DROP CONSTRAINT "Map_pkey";
       public            postgres    false    215            �           2606    16974    news_image news_image_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.news_image
    ADD CONSTRAINT news_image_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.news_image DROP CONSTRAINT news_image_pkey;
       public            postgres    false    218            �           2606    16976    news news_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.news DROP CONSTRAINT news_pkey;
       public            postgres    false    216            �           2606    16978    news_vote news_vote_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.news_vote
    ADD CONSTRAINT news_vote_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.news_vote DROP CONSTRAINT news_vote_pkey;
       public            postgres    false    220            �           2606    16980 &   product_clothing product_clothing_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.product_clothing
    ADD CONSTRAINT product_clothing_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.product_clothing DROP CONSTRAINT product_clothing_pkey;
       public            postgres    false    223            �           2606    16982     product_image product_image_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.product_image
    ADD CONSTRAINT product_image_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.product_image DROP CONSTRAINT product_image_pkey;
       public            postgres    false    226            �           2606    16984 ,   product_order_group product_order_group_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.product_order_group
    ADD CONSTRAINT product_order_group_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.product_order_group DROP CONSTRAINT product_order_group_pkey;
       public            postgres    false    229            �           2606    16986     product_order product_order_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.product_order
    ADD CONSTRAINT product_order_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.product_order DROP CONSTRAINT product_order_pkey;
       public            postgres    false    228            �           2606    16988    product product_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pkey;
       public            postgres    false    222            �           2606    16990    profile profile_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (user_id);
 >   ALTER TABLE ONLY public.profile DROP CONSTRAINT profile_pkey;
       public            postgres    false    232            �           2606    16992     project_image project_image_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.project_image
    ADD CONSTRAINT project_image_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.project_image DROP CONSTRAINT project_image_pkey;
       public            postgres    false    234            �           2606    16994 4   project_question_answer project_question_answer_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.project_question_answer
    ADD CONSTRAINT project_question_answer_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.project_question_answer DROP CONSTRAINT project_question_answer_pkey;
       public            postgres    false    237            �           2606    16996 4   project_question_option project_question_option_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.project_question_option
    ADD CONSTRAINT project_question_option_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.project_question_option DROP CONSTRAINT project_question_option_pkey;
       public            postgres    false    240            �           2606    16998 &   project_question project_question_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.project_question
    ADD CONSTRAINT project_question_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.project_question DROP CONSTRAINT project_question_pkey;
       public            postgres    false    236            �           2606    17000    project projects_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.project
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);
 ?   ALTER TABLE ONLY public.project DROP CONSTRAINT projects_pkey;
       public            postgres    false    233            �           2606    17002    user user_phone_number_key 
   CONSTRAINT     X   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_phone_number_key UNIQUE (phone);
 F   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_phone_number_key;
       public            postgres    false    243            �           2606    17004    user user_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pkey;
       public            postgres    false    243            �           2606    17006    user_points user_points_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.user_points
    ADD CONSTRAINT user_points_pkey PRIMARY KEY (user_id);
 F   ALTER TABLE ONLY public.user_points DROP CONSTRAINT user_points_pkey;
       public            postgres    false    245            �           2606    17008    vote vote_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.vote
    ADD CONSTRAINT vote_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.vote DROP CONSTRAINT vote_pkey;
       public            postgres    false    246            �           2606    17009    map Map_project_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.map
    ADD CONSTRAINT "Map_project_id_fkey" FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE;
 C   ALTER TABLE ONLY public.map DROP CONSTRAINT "Map_project_id_fkey";
       public          postgres    false    215    233    4755            �           2606    17014    news_vote news_id    FK CONSTRAINT     o   ALTER TABLE ONLY public.news_vote
    ADD CONSTRAINT news_id FOREIGN KEY (news_id) REFERENCES public.news(id);
 ;   ALTER TABLE ONLY public.news_vote DROP CONSTRAINT news_id;
       public          postgres    false    4737    220    216            �           2606    17019 "   news_image news_image_news_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.news_image
    ADD CONSTRAINT news_image_news_id_fkey FOREIGN KEY (news_id) REFERENCES public.news(id) ON DELETE CASCADE;
 L   ALTER TABLE ONLY public.news_image DROP CONSTRAINT news_image_news_id_fkey;
       public          postgres    false    218    216    4737            �           2606    17024 1   product_clothing product_clothing_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_clothing
    ADD CONSTRAINT product_clothing_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE NOT VALID;
 [   ALTER TABLE ONLY public.product_clothing DROP CONSTRAINT product_clothing_product_id_fkey;
       public          postgres    false    222    223    4743            �           2606    17029 +   product_image product_image_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_image
    ADD CONSTRAINT product_image_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.product_image DROP CONSTRAINT product_image_product_id_fkey;
       public          postgres    false    4743    222    226            �           2606    17034 *   product_order product_order_group_id_fkey1    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_order
    ADD CONSTRAINT product_order_group_id_fkey1 FOREIGN KEY (group_id) REFERENCES public.product_order_group(id) ON DELETE CASCADE NOT VALID;
 T   ALTER TABLE ONLY public.product_order DROP CONSTRAINT product_order_group_id_fkey1;
       public          postgres    false    228    4751    229            �           2606    17039 4   product_order_group product_order_group_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_order_group
    ADD CONSTRAINT product_order_group_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE NOT VALID;
 ^   ALTER TABLE ONLY public.product_order_group DROP CONSTRAINT product_order_group_user_id_fkey;
       public          postgres    false    229    243    4767            �           2606    17044 +   product_order product_order_product_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.product_order
    ADD CONSTRAINT product_order_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE SET NULL NOT VALID;
 U   ALTER TABLE ONLY public.product_order DROP CONSTRAINT product_order_product_id_fkey;
       public          postgres    false    4743    222    228            �           2606    17049 +   project_image project_image_project_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_image
    ADD CONSTRAINT project_image_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE;
 U   ALTER TABLE ONLY public.project_image DROP CONSTRAINT project_image_project_id_fkey;
       public          postgres    false    233    234    4755            �           2606    17054 >   project_question_answer project_question_answer_answer_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_question_answer
    ADD CONSTRAINT project_question_answer_answer_id_fkey FOREIGN KEY (answer_id) REFERENCES public.project_question_option(id) ON DELETE CASCADE;
 h   ALTER TABLE ONLY public.project_question_answer DROP CONSTRAINT project_question_answer_answer_id_fkey;
       public          postgres    false    237    240    4763            �           2606    17059 @   project_question_answer project_question_answer_question_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_question_answer
    ADD CONSTRAINT project_question_answer_question_id_fkey FOREIGN KEY (question_id) REFERENCES public.project_question(id) ON DELETE CASCADE;
 j   ALTER TABLE ONLY public.project_question_answer DROP CONSTRAINT project_question_answer_question_id_fkey;
       public          postgres    false    4759    237    236            �           2606    17064 <   project_question_answer project_question_answer_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_question_answer
    ADD CONSTRAINT project_question_answer_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;
 f   ALTER TABLE ONLY public.project_question_answer DROP CONSTRAINT project_question_answer_user_id_fkey;
       public          postgres    false    4767    243    237            �           2606    17069 @   project_question_option project_question_option_question_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_question_option
    ADD CONSTRAINT project_question_option_question_id_fkey FOREIGN KEY (question_id) REFERENCES public.project_question(id) ON DELETE CASCADE;
 j   ALTER TABLE ONLY public.project_question_option DROP CONSTRAINT project_question_option_question_id_fkey;
       public          postgres    false    4759    236    240            �           2606    17074 1   project_question project_question_project_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_question
    ADD CONSTRAINT project_question_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.project_question DROP CONSTRAINT project_question_project_id_fkey;
       public          postgres    false    236    233    4755            �           2606    17079    profile user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.profile
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;
 9   ALTER TABLE ONLY public.profile DROP CONSTRAINT user_id;
       public          postgres    false    4767    243    232            �           2606    17084    project user_id    FK CONSTRAINT     �   ALTER TABLE ONLY public.project
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;
 9   ALTER TABLE ONLY public.project DROP CONSTRAINT user_id;
       public          postgres    false    233    4767    243            �           2606    17089    news_vote user_id    FK CONSTRAINT     q   ALTER TABLE ONLY public.news_vote
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES public."user"(id);
 ;   ALTER TABLE ONLY public.news_vote DROP CONSTRAINT user_id;
       public          postgres    false    220    4767    243            �           2606    17094 $   user_points user_points_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_points
    ADD CONSTRAINT user_points_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;
 N   ALTER TABLE ONLY public.user_points DROP CONSTRAINT user_points_user_id_fkey;
       public          postgres    false    245    4767    243            �           2606    17099    vote vote_project_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.vote
    ADD CONSTRAINT vote_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.project(id) ON DELETE CASCADE;
 C   ALTER TABLE ONLY public.vote DROP CONSTRAINT vote_project_id_fkey;
       public          postgres    false    233    246    4755            �           2606    17104    vote vote_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.vote
    ADD CONSTRAINT vote_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id) ON DELETE CASCADE;
 @   ALTER TABLE ONLY public.vote DROP CONSTRAINT vote_user_id_fkey;
       public          postgres    false    243    246    4767            G   h   x�u̻1�X*��%�ۄ+���Ovj`W^=�b�
^���\�kK���f���S���؊cEk�d���m뻵�����Cm
�gn������0!�      H   �  x��S=s1�Ͽb��	&	%C��0�!�Ѭu�t�EZ�1��'9RP�r�Voߗ�����d����L�s.���DY�x[��!�3���{�5;�Wf�q�DK�b�i�eN{�K0*F�7�'�#���C`b���W��H�N��I�>yZ�c�L!fK�'y����4*�����iֺ�A�a�	��Q Vي>WH.&����Q�J2'%�� �/3�	�@)I�BN�?9A��eP6
�͜�QRw��+�ܭ/�o/.o.�׋�߯�����W�����W�����W��Cg������FI���y�Xw���oS,�X	|� {�x�� i8|$�E�s�{��v�U���.��ĺ�@��8e$�����I�{[�-�XC�����,�Heyc��t����0�-�n�m�tv�b6S	p�I�5�%���
�W�cڼi�� ++���nD�����4�V�p�-tl��<�=L��ݪx	�(4�����d�j����YE�K��!�@��K�<���Zu��V:(ȭ��:���bU�5�G�1�)�xx�xI�Eu�%xWUL�u
�*ǒ��F_�K�^͎pcsV��ɳ|�ڒn ��|�@0��	t-6���Z�#7rdEw'��4Kj�a�u&������y%��2-��=��&��w�x�ώ��v�N��G�'#�G:�fy��r���Z,��]      J   :   x�3��L�0�46�4׵HL5�51O�еL�4�M62M�HJ4627L�+�K����� '��      L   (   x�3�,�4�4"#]3]C.#��9���!W� �(]      N   g  x�uTˎ�J];_Ѭ�$Qf��0$��^�Ħb��"�nO?2����T�N`�DI?�ϫ��mu�NVbo�n�{	�l�-�ɴ>�w2�뚢xW�3v�6'��y���g�(�7��Ji���I������<d��Nf��A�ƵI����m�!��G��x>rM��[�1�h`לL�Q:W6���.X����#jQ˦��\������|�9������gj�u&y\����Kz�8�H�2Qn� �s�3��H�B����57GK{`g��g��U�, �qq@&dN�A�����rL���|~/c���e0�RC�YC�h	'�+ 
��/�[���z��mw��zw�j�{��~S��]]���,w*�-�6�r�,'�t�A���>���{ENW]�mWxCrpF����Z���8j��a���Z%a�?1MQ�m���3�!w[s�M�L�NRg��RJ��I�`����Dŉ�ܔ+��I�q�q��
���|��@F�|(
'p%.E�ὸ��:��
�[9�^��/��
��V(���S݂?
��tq�vAƵ!k��,b��~��Ġ],�}R=����g{�%t���͓�AE�Q�l}+;�ܬ�tB��֩����]I���y�(�����ƒ�;M�e��=�hq6�2���SA�ٕe��X
���e~�A��Q��q��=� �rE�ϧt^�)-��<2�u9(M]B�t5��F^M.��@�]�t ɽ�q��T�#�P���[O|i��ʮM!�A2
}�g	�i�0X
�(����2-K�w����˔��1�:�x��Bl]����U4�Խ@
�a^ͅ������<��~SNQ�޵�y�2�]�����B�v�{��Տ�j��ׄj�      O   K   x�3�44�V�V�QP� �DD�(��"Ssr��A�E��yJ�\��F�4A��N9�� :(5�3F��� Ʈ�      R   �   x��1�D!E�ؿ�p/� jgS��h���ԻXP�h�ԉ���!��]���~��ڌ���GS�`s8�"�8�խ�\:P�N���D� ���X�j)�%oo�f���s���{m�h_������عx���h��hk8]m�v#5<��h���{@�U�fV�����<���F�      T   {   x�UN9�0�w^�\G����~A��@C�p�D��c�pp�;7�^zy��h���Ҳ|~f-3s+��M��V
>K�������{�V=Ԗ`�um#�����v[M���t]�+�z��`d)|      U   �   x�3�42�4202�50�56����.��	s]SN׼��ԔT�0�Ԓ�Ԣ�ļ�b� G#KCK3c?�V�fP3Ӹ-�%���8ad �0I ��HL�.NJ-JO-*.)JL,Q09��H�7�jq	W� ��7v      X   �   x�=�A
�0EדS���Dk���"ܺ	�TCC���M]����	�,�W���4}4��`ʼr�1���_�
�("�	�&~vq�:��.]]`X��Z�),pB�&����Tĕ:���K��Rg��G�J� '*�      Y   �  x��T�n�0<�_�ȥٰ�r�4�hR��E�$������K=���l/wfg�{̒�F� g.po����[xΠ��T���6V���u��� ��F��ra�{ԶmЄ�VZg���Z/4�x��S�B� Җ3*(C5|$$L~��E,4�r�0���D�4�1�A�ab����v���IX9����E�#���K'|p���
1���i$Ct�2�JT�RxC;����hj��wB��jUbTҡW���\+���jl�Y�,�T<#���Q������J�w�I1�.)�,K�}v��?l�������~�����C��$$as|��c�m�t�9?t�-�7�}F��΃4�읪�8[c�w��)fy�GI�L�Q<�ކ��c3�E:)��:�*F�V�0&I��t�AJߴ���LdF�� K#�V��w�z�>��k+	���!v��^���_�U��`��n��i�%$��S��l��ԑCZ%�hP��J!�j�gɊGG�!����:7���q��*�:�%�R�[!���W�y$�������;��B�:2��Zos��h�땐�b�FH����H}��Y17!:�
E7�BM�䝇*��(�p��X/ܞgn|\������k�+:S����#��e�%Td��\x���c��,k)y��jK�W9A���K�y�]�Z�<m}����e�L��i	���@�����(�5w��c���q�knq�YE��n�����0�      Z   �   x�-�1�1���s"0��l���n��W�L�AB���K!,6�
}zfE�8��K�A����5�ȣB�.��!hʄ�M�2��=ZQٍ&�6^|.x� =�[�n��j�^�W����ULsI�շ,��vsۺ�v�H���Y�L��9rR�]�o>�.Rf�r�[2��Fi:�i���({o�!��p��m� ��@��y]��sZR�      \   �   x�}ͽ�0�ṽ�s&�ur0&�..Ş�I����^`���[��Q+q�5e��k
0$��x>�� �F�q�p,��0��̱@ar�ƭ����� ���%,9��COb�`p�mL~1����Z�\��R0�cfxiWt�b�w����^�y�JU��L���NJ�G�M�      ]   ,   x�ƹ   ���|wq�9�Tф�|цǿ+ԡ�������      `     x�=O�j1<[_�/(]o�Ǳ�
�B�c/�V�5������+'m���ь��z�m�k<dW$W���M#���B�VP�w�3�tZ8
�{�=������Z�����_�����m��H����p�Ê)cl6���l�� %�`�E|�tR1�0;�)��&~!�RP��>x�����%լ�CS�P-E�xH�.�=HiYj��"��M|[��v����iv1r�&���	��ˌ��cWeNً׸�7��xq#��[�����G���_�u�      c     x�]��n�@ @�5|�&�<�).���HA�yU���/Ѹ�������AC�e���a�a`H[#�c\�1���ЉŶ�]G$٢�4x!;gV	�u�r��	~r��io.*^=n�yom��������x`] �dg�%-�H=��eW�L~q���CԆg����yO`_���.�H�X�u�����b�]�`\ =dB��t
�7g6c8\�5[�r�ka���/�X`�:���[y���ݢ����&����o ����YY      e   $   x�32�44�26�ƜF\FF���\1z\\\ DD      f   #   x�36�46�42�4202�50�5��,����� ?n     